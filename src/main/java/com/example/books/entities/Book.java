package com.example.books.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name="books")
public class Book {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	@Size(max = 30)
	private String title;
	
	@NotBlank
	@Size(max = 50)
	private String isbn;
	
	@NotBlank
	@Size(max =50)
	private Integer editionNumber;
	
	@NotBlank
	@Size(max =50)
	private Integer yearofpublish;
	
	@OneToMany(mappedBy = "books",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	private Set<Category> category;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Integer getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(Integer editionNumber) {
		this.editionNumber = editionNumber;
	}

	public Integer getYearofpublish() {
		return yearofpublish;
	}

	public void setYearofpublish(Integer yearofpublish) {
		this.yearofpublish = yearofpublish;
	}
	

}
