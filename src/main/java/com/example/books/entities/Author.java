package com.example.books.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name="authors")
public class Author {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	@Size(max = 30)
	private String first_name;
	
	@NotBlank
	@Size(max = 30)
	private String last_name;
	
	@NotBlank
	@Size(max = 30)
	private String currentresidence;
	
	@NotBlank
	@Size(max = 10)
	private Integer yearborn;
	
	@OneToMany(mappedBy = "authors",cascade = CascadeType.ALL)
	private Set<Book> books;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getCurrentresidence() {
		return currentresidence;
	}

	public void setCurrentresidence(String currentresidence) {
		this.currentresidence = currentresidence;
	}

	public Integer getYearborn() {
		return yearborn;
	}

	public void setYearborn(Integer yearborn) {
		this.yearborn = yearborn;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}
	
	
	
	
	
}
